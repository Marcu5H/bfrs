# BFRS

Simple brainfuck compiler.

## Performance

| Program   | Execution time |
|-----------|----------------|
|Mandelbrot | `900 ms`       |
