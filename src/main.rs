use std::{collections::HashMap, env, fs::File, io::Read, process::exit};

enum Instructions {
    DPInc(usize),
    DPDec(usize),
    Inc(usize),
    Dec(usize),
    Out,
    In,
    OJump,
    CJump,
}

fn print_help_and_die() {
    println!("Usage: bfrs [file]");
    exit(1);
}

fn get_jumps(
    instructions: &Vec<Instructions>,
) -> Option<(HashMap<usize, usize>, HashMap<usize, usize>)> {
    let mut open: HashMap<usize, usize> = HashMap::new();
    let mut close: HashMap<usize, usize> = HashMap::new();

    let mut open_stack: Vec<usize> = Vec::new();
    for (index, instruction) in instructions.iter().enumerate() {
        match instruction {
            Instructions::OJump => open_stack.push(index),
            Instructions::CJump => {
                if let Some(opened_at) = open_stack.pop() {
                    open.insert(opened_at, index);
                    close.insert(index, opened_at);
                } else {
                    return None;
                }
            }
            _ => {}
        }
    }

    return Some((open, close));
}

fn get_instructions(code: &String) -> Vec<Instructions> {
    let mut instrs: Vec<Instructions> = Vec::new();
    let mut code_iterator = code.chars().peekable();
    while let Some(ch) = code_iterator.next() {
        match ch {
            '>' => {
                let mut c = 1;
                while code_iterator.peek() == Some(&'>') {
                    c += 1;
                    code_iterator.next();
                }
                instrs.push(Instructions::DPInc(c));
            }
            '<' => {
                let mut c = 1;
                while code_iterator.peek() == Some(&'<') {
                    c += 1;
                    code_iterator.next();
                }
                instrs.push(Instructions::DPDec(c));
            }
            '+' => {
                let mut c = 1;
                while code_iterator.peek() == Some(&'+') {
                    c += 1;
                    code_iterator.next();
                }
                instrs.push(Instructions::Inc(c));
            }
            '-' => {
                let mut c = 1;
                while code_iterator.peek() == Some(&'-') {
                    c += 1;
                    code_iterator.next();
                }
                instrs.push(Instructions::Dec(c));
            }
            '.' => instrs.push(Instructions::Out),
            ',' => instrs.push(Instructions::In),
            '[' => instrs.push(Instructions::OJump),
            ']' => instrs.push(Instructions::CJump),
            _ => {}
        }
    }

    instrs
}

fn run_bf(code: &String) -> i32 {
    let instructions = get_instructions(code);

    let (open, close) = match get_jumps(&instructions) {
        Some(h) => h,
        None => {
            eprintln!("Failed to get jumps");
            return 1;
        }
    };

    println!("    global _start");
    println!("    section .data");
    println!("_data db 30000 dup(0)");
    println!("    section .text");
    println!("_start:");
    println!("    xor r8d, r8d");

    let mut ins_counter: usize = 0;
    while ins_counter < instructions.len() {
        match instructions[ins_counter] {
            Instructions::DPInc(v) => {
                println!("    add r8d, {v}");
            }
            Instructions::DPDec(v) => {
                println!("    sub r8d, {v}");
            }
            Instructions::Inc(v) => {
                println!("    lea rax, [_data + r8d]");
                println!("    add [rax], byte {v}");
            }
            Instructions::Dec(v) => {
                println!("    lea rax, [_data + r8d]");
                println!("    sub [rax], byte {v}");
            }
            Instructions::Out => {
                println!("    xor rax, rax");
                println!("    inc rax");
                println!("    xor rdi, rdi");
                println!("    inc rdi");
                println!("    lea rsi, [_data + r8d]");
                println!("    mov rdx, 1");
                println!("    syscall");
            }
            Instructions::In => {
                println!("    xor rax, rax");
                println!("    xor rdi, rdi");
                println!("    lea rsi, [_data + r8d]");
                println!("    mov rdx, 1");
                println!("    syscall");
            }
            Instructions::OJump => {
                println!("_ic_{ins_counter}:");
                println!("    movzx rax, byte [_data +r8d]");
                println!("    test rax, rax");
                println!("    jz _ic_exit_{}", open.get(&ins_counter).unwrap());
            }
            Instructions::CJump => {
                println!("    movzx rax, byte [_data + r8d]");
                println!("    test rax, rax");
                println!("    jnz _ic_{}", close.get(&ins_counter).unwrap());
                println!("_ic_exit_{ins_counter}:");
            }
        }
        ins_counter += 1;
    }

    println!("    mov rax, 60");
    println!("    xor rdi, rdi");
    println!("    syscall");

    0
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        print_help_and_die();
    }

    let mut bfile = match File::open(&args[1]) {
        Ok(f) => f,
        Err(e) => {
            eprintln!("Failed to open {}: {}", args[1], e);
            exit(1);
        }
    };

    let mut bfbuf: String = String::new();
    if let Err(e) = bfile.read_to_string(&mut bfbuf) {
        eprintln!("Failed to read from file: {}", e);
        exit(1);
    }

    exit(run_bf(&bfbuf));
}
