# F = adding_two_values.bf
# F = test.bf
# F = hello_world.bf
F = mandelbrot.bf

default: build comp

build:
	cargo build
comp:
	./target/debug/bfrs $(F) > $(F).asm
	nasm -f elf64 -Oxv -g -w+x -o a.out $(F).asm
	ld a.out -o $(F).elf
